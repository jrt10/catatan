# Build an iRedMail fail-over Cluster with KeepAlived, HAProxy, GlusterFS, OpenLDAP, Mariadb

[TOC]

November, 2017 

## Video tutor

[![Video tutor](https://i.ytimg.com/vi/NFLXB0vl9Io/hqdefault.jpg?sqp=-oaymwEXCPYBEIoBSFryq4qpAwkIARUAAIhCGAE=&rs=AOn4CLAGVACOrispIyknnVTXi7gVmsG1zQ)](https://youtu.be/NFLXB0vl9Io) 

## TODO

* Use clear server hostnames and IP addresses for all involved servers.
* Install adminer from <http://adminer.org>

## Goal

Build a fail-over cluster with 4 servers (2 backend servers behind HAProxy + KeepAlived).

## Requirements

* A valid mail domain name. We use `example.com` as mail domain name in this document.
* 4 servers, all are CentOS 7.

    * 2 servers run HAProxy + KeepAlived as a frontend for load-balance
      (HAProxy) and fail-over (KeepAlived).
    * 2 servers run the actual mail services. We will install the latest
      iRedMail release for this.

The big picture:

![](https://bytebucket.org/jrt10/catatan/raw/master/iredmailhat10a.bmp)

## Summary

Hostnames and IP addresses:

* We use hostname `ha1.example.com` and `ha2.example.com` for our 2 servers
  which runs HAProxy and KeepAlived, use `ha1` and `ha2` for short.

* We use hostname `mail1.example.com` and `mail2.example.com` for our 2 servers
  which runs iRedMail for mail services, use `mail1` and `mail2` for short.

* IP addresses:

```
192.168.100.1 ha1
192.168.100.2 ha2
192.168.100.3 mail1
192.168.100.4 mail2
```

The procedure:

1. Install and configure GlusterFS as glusterserver & glusterclient (you can
   use separate machine for glusterserver) it's better to use a new hard drive
   with the same capacity
1. Install and configure iRedMail
1. Setup OpenLDAP replication (Master-Slave)
1. Setup MariaDB replication (Master-Master)
1. Install and configure KeepAlived
1. Install and configure HAProxy
1. Configure nginx on iRedMail servers

## GlusterFS

### Add new hard disk and format with preferred file system

first, add new hard drive with the same capacity

* on both servers, update `/etc/hosts`:

```
192.168.100.3 mail1
192.168.100.4 mail2
```

* add new disk on `mail1`:

type 'n', and hit enter for next question, (dont forget to write) hit 'w'

```
fdisk /dev/sdb
/sbin/mkfs.ext4 /dev/sdb1
mkdir /glusterfs1
```

Update `/etc/fstab`:

```
/dev/sdb1 /glusterfs1      ext4    defaults        1 2
```

remount all:

```
mount -a
```

* add new disk on mail2:

type 'n', and hit enter for next question, (dont forget to write) hit 'w'

```
fdisk /dev/sdb
/sbin/mkfs.ext4 /dev/sdb1
mkdir /glusterfs2
```

Update /etc/fstab:

```
/dev/sdb1 /glusterfs1      ext4    defaults        1 2
```

remount all

```
mount -a
```

### Install and Configure GulsterFS

* on both servers (mail1 & mail2):

```
yum -y install centos-release-gluster
yum -y install glusterfs glusterfs-fuse glusterfs-server
```

activate the service

```
systemctl enable glusterd.service
systemctl start glusterd.service
```

disabling firewall

```
systemctl stop firewalld.service
systemctl disable firewalld.service
```

* on mail1:

```
gluster peer probe mail2
```

* on mail2:

```
gluster peer probe mail1
```

you can check status with command below:

```
gluster peer status
```

* ONLY on mail1:

```
gluster volume create mailrep-volume replica 2  mail1:/glusterfs1/vmail  mail2:/glusterfs2/vmail force
gluster volume start mailrep-volume
```

check it

```
gluster volume info mailrep-volume
```

* create folder for vmail and mount glusterfs to vmail folder

on mail1:

```
mkdir  /var/vmail
mount.glusterfs mail1:/mailrep-volume /var/vmail/
```

Update /etc/fstab

```
mail1:/mailrep-volume /var/vmail glusterfs defaults,_netdev 0 0
```

remount all

```
mount -a
```

check it

```
df -h
```

* on mail2:

```
mkdir  /var/vmail
mount.glusterfs mail2:/mailrep-volume /var/vmail/
```

Update /etc/fstab:

```
mail2:/mailrep-volume /var/vmail glusterfs defaults,_netdev 0 0
```

remount all

```
mount -a
```

check it

```
df -h
```

you can test it by creating any files on one of your mail servers

```
cd /var/vmail; touch R1 R2 R3 R4 R5 R6
```

make sure it, by checking files on both servers

```
ls -la /var/vmail
```

## Install and configure iRedMail

* Install the latest iRedMail on 2 servers (mail1 & mail2)

* For installing iRedMail on CentOS, please check its installation guide:
  [Install iRedMail on Red Hat Enterprise Linux, CentOS](./install.iredmail.on.rhel.html)

!!! note

    * install iRedMail on `mail1` first, after mail1 finish you can install it
      to mail2 (better do not reboot after installing iRedMail, wait untill
      finish install/configure)

    * Dont forget to choose LDAP and using default mail folder: `/var/vmail`
    * Choose Nginx as web server


## Configure LDAP replication (Master-Slave)

* on mail1 (MASTER), update `/etc/openldap/slapd.conf`:
* for binddn & credentials please see iRedMail.tips
* uncomment `moduleload syncprov.la`

```
overlay syncprov 
syncprov-checkpoint 99 9 
syncprov-sessionlog 99 
#Global
section serverID 1 
#database section
#syncrepl directive
syncrepl rid=001 
			provider=ldap://mail2 
            bindmethod=simple
			binddn="cn=vmail,dc=example,dc=com" 
			credentials=vmail_pass_of_mail2
			searchbase="o=domains,dc=example,dc=com" 
			schemachecking=on
			type=refreshAndPersist 
			retry="60 +"
mirrormode on
```

* on mail2 (SLAVE), update `/etc/openldap/slapd.conf`:

```
overlay syncprov
syncprov-checkpoint 100 10
syncprov-sessionlog 100
#Global section
serverID    2
#database section
#syncrepl directive
syncrepl      rid=001
              provider=ldap://mail1
              bindmethod=simple
              binddn="cn=vmail,dc=example,dc=com"
              credentials=vmail_pass_of_mail1
              searchbase="o=domains,dc=example,dc=com"
              schemachecking=on
              type=refreshAndPersist
              retry="60 +"
mirrormode on
```

on both servers set firewalld to accept gluster port, ldap port, and database to each servers,
or you can set by your own rules:

```
firewall-cmd --permanent \
     --zone=iredmail \
     --add-rich-rule='rule family="ipv4" source address="192.168.100.3/24" port protocol="tcp" port="389" accept'

firewall-cmd --permanent \
    --zone=iredmail \
    --add-rich-rule='rule family="ipv4" source address="192.168.100.4/24" port protocol="tcp" port="3306" accept'

firewall-cmd --zone=iredmail --permanent --add-port=111/udp
firewall-cmd --zone=iredmail --permanent --add-port=24007/tcp
firewall-cmd --zone=iredmail --permanent --add-port=24008/tcp
firewall-cmd --zone=iredmail --permanent --add-port=24009/tcp
firewall-cmd --zone=iredmail --permanent --add-port=139/tcp
firewall-cmd --zone=iredmail --permanent --add-port=445/tcp
firewall-cmd --zone=iredmail --permanent --add-port=965/tcp
firewall-cmd --zone=iredmail --permanent --add-port=2049/tcp
firewall-cmd --zone=iredmail --permanent --add-port=38465-38469/tcp
firewall-cmd --zone=iredmail --permanent --add-port=631/tcp
firewall-cmd --zone=iredmail --permanent --add-port=963/tcp
firewall-cmd --zone=iredmail --permanent --add-port=49152-49251/tcp
```

reload firewall rules:

```
firewall-cmd --complete-reload
```

Restart OpenLDAP service:

```
systemctl restart slapd
```

## Configure MariaDB replication (Master-Master)

* on mail1, update `/etc/my.cnf`:

```
server-id                   = 1
    log_bin                 = /var/log/mariadb/mariadb-bin.log
    log-slave-updates
    log-bin-index           = /var/log/mariadb/log-bin.index
    log-error               = /var/log/mariadb/error.log
    relay-log               = /var/log/mariadb/relay.log
    relay-log-info-file     = /var/log/mariadb/relay-log.info
    relay-log-index         = /var/log/mariadb/relay-log.index
    auto_increment_increment = 10
    auto_increment_offset   = 1
    binlog_do_db            = amavisd
    binlog_do_db            = iredadmin
    binlog_do_db            = roundcubemail
    binlog_do_db            = sogo
    binlog-ignore-db=test
    binlog-ignore-db=information_schema
    binlog-ignore-db=mysql
    binlog-ignore-db=iredapd
    log-slave-updates
    replicate-ignore-db=test
    replicate-ignore-db=information_schema
    replicate-ignore-db=mysql
    replicate-ignore-db=iredapd
```

Restart MariaDB service:

```
systemctl restart mariadb
```

*on mail2, update `/etc/my.cnf`:

```
server-id                   = 2
    log_bin                 = /var/log/mariadb/mariadb-bin.log
    log-slave-updates
    log-bin-index           = /var/log/mariadb/log-bin.index
    log-error               = /var/log/mariadb/error.log
    relay-log               = /var/log/mariadb/relay.log
    relay-log-info-file     = /var/log/mariadb/relay-log.info
    relay-log-index         = /var/log/mariadb/relay-log.index
    auto_increment_increment = 10
    auto_increment_offset = 1
    binlog_do_db            = amavisd
    binlog_do_db            = iredadmin
    binlog_do_db            = roundcubemail
    binlog_do_db            = sogo
    binlog-ignore-db=test
    binlog-ignore-db=information_schema
    binlog-ignore-db=mysql
    binlog-ignore-db=iredapd
    log-slave-updates
    replicate-ignore-db=test
    replicate-ignore-db=information_schema
    replicate-ignore-db=mysql
    replicate-ignore-db=iredapd
```

Restart MariaDB service:

```
systemctl restart mariadb
```

### create replicator dbuser on both servers ##

* on mail1, login as MariaDB root user, then execute sql commands below:

```
create user 'replicator'@'%' identified by '12345678';
grant replication slave on *.* to 'replicator'@'%';
SHOW MASTER STATUS;
+--------------------+----------+----------------------------------------------+-------------------------------+
| File               | Position | Binlog_Do_DB                                 | Binlog_Ignore_DB              |
+--------------------+----------+----------------------------------------------+-------------------------------+
| mariadb-bin.000001 |      245 | amavisd,iredadmin,iredapd,roundcubemail,sogo | test,information_schema,mysql |
+--------------------+----------+----------------------------------------------+-------------------------------+
```

check master status in column `File` and `Position`:

* on mail2:

```
create user 'replicator'@'%' identified by '12345678';
grant replication slave on *.* to 'replicator'@'%';
slave stop;
CHANGE MASTER TO MASTER_HOST = '192.168.100.3', MASTER_USER = 'replicator', MASTER_PASSWORD = '12345678', MASTER_LOG_FILE = 'mariadb-bin.000001', MASTER_LOG_POS = 245;
slave start;
SHOW MASTER STATUS;
+--------------------+----------+----------------------------------------------+-------------------------------+
| File               | Position | Binlog_Do_DB                                 | Binlog_Ignore_DB              |
+--------------------+----------+----------------------------------------------+-------------------------------+
| mariadb-bin.000001 |     289 | amavisd,iredadmin,iredapd,roundcubemail,sogo | test,information_schema,mysql |
+--------------------+----------+----------------------------------------------+-------------------------------+

show slave status\G;
```

* change to your own master status MASTER_LOG_FILE is from `File`, MASTER_LOG_POS is from `Position` of master mail1
* check master status for `File` and `Position`

Restart MariaDB service:

```
systemctl restart mariadb
```

* on mail1, login as MariaDB root user:

```
slave stop;
CHANGE MASTER TO MASTER_HOST = '192.168.100.4', MASTER_USER = 'replicator', MASTER_PASSWORD = '12345678', MASTER_LOG_FILE = 'mariadb-bin.000001', MASTER_LOG_POS = 289;
slave start;
show slave status\G;
exit;
```

* change to your own master status MASTER_LOG_FILE is from `File`, MASTER_LOG_POS is from `Position` of master mail2*.

Restart MariaDB service:

```
systemctl restart mariadb
```

* reboot one of mailserver and wait till up, then reboot the other mailserver

## Testing

* u can create users using iredadmin on mail1, then check users from mail2 and you can see its already sync
* try to login using roundcubemail from any mailserver then u can check users on database 'roundcubemail->users', and its already sync
* only mail1 'can add n modify' users
* this mailservers act as Glusterserver & Glusterclient, if u want to reboot the servers, please reboot first server untill this up then reboot the second server.
* if all servers are reboot for the same time it will not mounting '/var/vmail' folder. u must force mount manually using this command 'gluster volume start mailrep-volume force'


To view the DB easily, you may want to install adminer from <http://adminer.org/> (it's web-based SQL management tool, just a single PHP file):


## Install and configure KeepAlived

Install on 2 servers (ha1 & ha2)

* on both servers, update `/etc/hosts`:

```
192.168.100.1 ha1
192.168.100.2 ha2
192.168.100.3 mail1
192.168.100.4 mail2
```

* Install KeepAlived and backup default config file:

```
yum install -y keepalived ipset-libs
mv /etc/keepalived/keepalived.conf /etc/keepalived/keepalived.conf_DEFAULT
```

* on ha1:

```
nano /etc/keepalived/keepalived.conf
```

* change eth0 to your existing interface

```
vrrp_script chk_haproxy {
    script "killall -0 haproxy" # check the haproxy process
    interval 2 # every 2 seconds
    weight 2 # add 2 points if OK
}

vrrp_instance VI_1 {
    interface eth0 # interface to monitor
    state MASTER # MASTER on ha1, BACKUP on ha2
    virtual_router_id 51
    priority 101 # 101 on ha1, 100 on ha2
    virtual_ipaddress {
    192.168.100.10 # virtual ip address
    }
    track_script {
        chk_haproxy
    }
}
```

* on ha2, update `/etc/keepalived/keepalived.conf`

change `eth0` to your existing interface

```
vrrp_script chk_haproxy {
    script "killall -0 haproxy" # check the haproxy process
    interval 2 # every 2 seconds
    weight 2 # add 2 points if OK
}
vrrp_instance VI_1 {
    interface eth0 # interface to monitor
    state BACKUP # MASTER on ha1, BACKUP on ha2
    virtual_router_id 51
    priority 101 # 101 on ha1, 100 on ha2
    virtual_ipaddress {
    192.168.100.10 # virtual ip address
    }
    track_script {
        chk_haproxy
    }
}
```

* activate KeepAlived service on both servers:

```
systemctl enable keepalived
systemctl start keepalived
```

* Check status of virtual IP (192.168.100.10) with command below:

```
systemctl status keepalived
```

## Install and configure HAProxy

* on both servers update `/etc/sysctl.conf`

```
net.ipv4.ip_nonlocal_bind=1
```

* Install on both servers (ha1 & ha2)

```
yum install gcc pcre-static pcre-devel openssl-devel mod_ssl -y
wget http://www.haproxy.org/download/1.7/src/haproxy-1.7.9.tar.gz
tar -zxvf haproxy-1.7.9.tar.gz

cd haproxy-1.7.9
make TARGET=linux2628 USE_PCRE=1 USE_OPENSSL=1 USE_ZLIB=1 USE_CRYPT_H=1 USE_LIBCRYPT=1 && make install

mkdir -p /etc/haproxy
mkdir -p /var/lib/haproxy 
touch /var/lib/haproxy/stats
ln -s /usr/local/sbin/haproxy /usr/sbin/haproxy
cp examples/haproxy.init /etc/init.d/haproxy
chmod 755 /etc/init.d/haproxy
systemctl daemon-reload
chkconfig haproxy on
useradd -r haproxy
```
check it 

```
haproxy -v
```

* on ha1: update `/etc/haproxy/haproxy.cfg`

```
global  
        log 127.0.0.1   local0  
        log 127.0.0.1   local1 debug  
        maxconn   45000 #Total Max Connections.  
        daemon  
        nbproc      1 #Number of processing cores.  
        tune.ssl.default-dh-param 2048  
defaults  
        timeout server 86400000  
        timeout connect 86400000  
        timeout client 86400
listen premierdis-http
		bind 192.168.100.10:80
		mode http
		redirect scheme https if !{ ssl_fc }
listen premierdis
		bind 192.168.100.10:443 ssl crt /etc/ssl/iredmail.org/iredmail.org.pem
		mode http
		stats enable
		stats uri /haproxy?stats
		stats realm STATS
		stats auth user:pass
		balance source
		option http-server-close
		timeout http-keep-alive 3000
		reqidel ^X-Real-IP:
		option forwardfor header X-Real-IP
		reqadd X-Forwarded-Proto:\ https
		server mail2 192.168.100.3:80 check
		server mail2 192.168.100.4:80 check
frontend ft_http
        bind :80
        mode http
        default_backend bk_http
frontend ft_https
        bind :443 ssl crt /etc/ssl/iredmail.org/iredmail.org.pem
        mode tcp
        default_backend bk_https
backend bk_http
        mode http
        balance roundrobin
        stick on src table bk_https
        default-server inter 1s
        server mail1 192.168.100.3:80 check id 1
        server mail2 192.168.100.4:80  check id 2
backend bk_https
        mode tcp
        balance roundrobin
        stick-table type ip size 200k expire 30m
        stick on src
        default-server inter 1s
        server mail1 192.168.100.3:443 check id 1
        server mail2 192.168.100.4:443 check id 2
# Reporting
listen stats
		bind :9000
		mode http
		stats enable
		stats hide-version
		stats realm Authorization
		# URI of the stats page: localhost:9000/haproxy_stats
		stats uri /haproxy_stats
		stats auth yourUsername:yourPassword		
```


* on ha2, update `/etc/haproxy/haproxy.cfg`

```
global  
        log 127.0.0.1   local0  
        log 127.0.0.1   local1 debug  
        maxconn   45000 #Total Max Connections.  
        daemon  
        nbproc      1 #Number of processing cores.  
        tune.ssl.default-dh-param 2048  
defaults  
        timeout server 86400000  
        timeout connect 86400000  
        timeout client 86400
listen premierdis-http
		bind 192.168.100.10:80
		mode http
		redirect scheme https if !{ ssl_fc }
listen premierdis
		bind 192.168.100.10:443 ssl crt /etc/ssl/iredmail.org/iredmail.org.pem
		mode http
		stats enable
		stats uri /haproxy?stats
		stats realm STATS
		stats auth user:pass
		balance source
		option http-server-close
		timeout http-keep-alive 3000
		reqidel ^X-Real-IP:
		option forwardfor header X-Real-IP
		reqadd X-Forwarded-Proto:\ https
		server mail2 192.168.100.3:80 check
		server mail2 192.168.100.4:80 check
frontend ft_http
        bind :80
        mode http
        default_backend bk_http
frontend ft_https
        bind :443 ssl crt /etc/ssl/iredmail.org/iredmail.org.pem
        mode tcp
        default_backend bk_https
backend bk_http
        mode http
        balance roundrobin
        stick on src table bk_https
        default-server inter 1s
        server mail1 192.168.100.3:80 check id 1
        server mail2 192.168.100.4:80  check id 2
backend bk_https
        mode tcp
        balance roundrobin
        stick-table type ip size 200k expire 30m
        stick on src
        default-server inter 1s
        server mail1 192.168.100.3:443 check id 1
        server mail2 192.168.100.4:443 check id 2
# Reporting
listen stats
		bind :9000
		mode http
		stats enable
		stats hide-version
		stats realm Authorization
		# URI of the stats page: localhost:9000/haproxy_stats
		stats uri /haproxy_stats
		stats auth yourUsername:yourPassword	
```

* on both servers:

create cert for ssl redirect (to iRedMail Servers)

```
mkdir /etc/ssl/iredmail.org/
openssl genrsa -out /etc/ssl/iredmail.org/iredmail.org.key 2048
openssl req -new -key /etc/ssl/iredmail.org/iredmail.org.key -out /etc/ssl/iredmail.org/iredmail.org.csr
openssl x509 -req -days 365 -in /etc/ssl/iredmail.org/iredmail.org.csr -signkey /etc/ssl/iredmail.org/iredmail.org.key -out /etc/ssl/iredmail.org/iredmail.org.crt
cat /etc/ssl/iredmail.org/iredmail.org.crt /etc/ssl/iredmail.org/iredmail.org.key > /etc/ssl/iredmail.org/iredmail.org.pem
```

activate HAProxy service

```
systemctl enable haproxy
systemctl start haproxy
```

check log if any errors

```
tail -f /var/log/messages
systemctl status haproxy
```

allow http, https, haproxystat ports

```
firewall-cmd --zone=public --permanent --add-port=80/tcp
firewall-cmd --zone=public --permanent --add-port=443/tcp
firewall-cmd --zone=public --permanent --add-port=9000/tcp
firewall-cmd --complete-reload
```

## Configure nginx on iRedMail servers

* on both servers, update `/etc/nginx/sites-conf.d/default/0-root.conf`:

*disabling ssl redirect on both mailserver by default (non https & start using haproxy ssl cert)

```
root /var/www/roundcubemail;
```

Restart nginx service

```
systemctl restart uwsgi && systemctl restart nginx
```

## Testing

* For HA Testing, u can try to shutdown one of your server to testing it (ha1 or ha2 --/OR-- mail1 or mail2)
* try check virtual ip address on your browser