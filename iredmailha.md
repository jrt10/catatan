# HA iRedmail LDAP+Nginx (Centos 7), using: KeepAlived+HAProxy,GlusterFS, syncLDAP, MariadbReplica #

**REQUIRED :** 4 Servers (Centos 7)  
**GOAL :** 2 iRedmail Servers behind 2 Keepalived+HAProxy  
**EXAMPLE DOMAIN :** iredmail.org

![](http://s14.postimg.org/sfooxggu9/iredmailhat10b.png)


**STEPS :**  

1. Install+Configure KeepAlived
2. Install+Configure HAProxy
3. Install+Configure GlusterFS as glusterserver & glusterclient  (u can use separate machine for glusterserver) it's better to use a new hard drive with the same capacity  
4. Install+Configure Iredmail -> sync LDAP(Master-Slave), replicate MariDB (Master-Master)


#1. Install+Configure KeepAlived #
**Install on 2 servers (ha1 & ha2)*

***on both servers:***

>[root@ ~] nano /etc/hosts

```
192.168.1.1 ha1
192.168.1.2 ha2
192.168.1.3 mail1
192.168.1.4 mail2
``` 
>[root@ ~] yum install -y keepalived  
>[root@ ~] mv /etc/keepalived/keepalived.conf /etc/keepalived/keepalived.conf_DEFAULT
 
***on ha1:***  
>[root@ha1 ~] nano /etc/keepalived/keepalived.conf

**change eth0  to your existing interface*
```
vrrp_script chk_haproxy {
    script "killall -0 haproxy" # check the haproxy process
    interval 2 # every 2 seconds
    weight 2 # add 2 points if OK
}
vrrp_instance VI_1 {
    interface eth0 # interface to monitor 
    state MASTER # MASTER on ha1, BACKUP on ha2
    virtual_router_id 51
    priority 101 # 101 on ha1, 100 on ha2
    virtual_ipaddress {
    192.168.1.10 # virtual ip address 
    }
    track_script {
        chk_haproxy
    }
} 
```

***on ha2:***  
>[root@ha2 ~] nano /etc/keepalived/keepalived.conf

**change eth0  to your existing interface* 
```
vrrp_script chk_haproxy {
    script "killall -0 haproxy" # check the haproxy process
    interval 2 # every 2 seconds
    weight 2 # add 2 points if OK
}
vrrp_instance VI_1 {
    interface eth0 # interface to monitor 
    state BACKUP # MASTER on ha1, BACKUP on ha2
    virtual_router_id 51
    priority 101 # 101 on ha1, 100 on ha2
    virtual_ipaddress {
    192.168.1.10 # virtual ip address 
    }
    track_script {
        chk_haproxy
    }
} 
```

***on both servers:***  

activate KeepAlived service  

>[root@ ~] systemctl enable keepalived  
>[root@ ~] systemctl start keepalived  
 
cek status of virtual IP (192.168.1.10) using this command  
>[root@ ~] ip a


#2. Install+Configure HAProxy #
**Install on 2 servers (ha1 & ha2)*

***on both servers:***              
>[root@ ~] yum install -y haproxy                
>[root@ ~] mv /etc/haproxy/haproxy.cfg /etc/haproxy/haproxy.cfg_DEFAULT

***on ha1:*** 
>[root@ha1 ~] nano /etc/haproxy/haproxy.cfg

```
global
        log 127.0.0.1   local0
        log 127.0.0.1   local1 debug
        maxconn   45000 # Total Max Connections.
        daemon
        nbproc      1 # Number of processing cores.
defaults
        timeout server 86400000
        timeout connect 86400000
        timeout client 86400000
        timeout queue   1000s
# [HTTP Site Configuration]
listen  http_web 192.168.1.10:80
                bind *:80
                bind *:443 ssl crt /etc/ssl/iredmail.org/iredmail.org.pem
            redirect scheme https if !{ ssl_fc }
        mode http
        balance roundrobin  # Load Balancing algorithm
        option httpchk
        option forwardfor
        server mail1 192.168.1.3:443 weight 1 maxconn 512 check
        server mail2 192.168.1.4:443 weight 1 maxconn 512 check
# [HTTPS Site Configuration]
listen  https_web 192.168.1.10:443
        mode tcp
        balance source# Load Balancing algorithm
        reqadd X-Forwarded-Proto:\ http
        server mail1 192.168.1.3:443 weight 1 maxconn 512 check
        server mail2 192.168.1.4:443 weight 1 maxconn 512 check
# Reporting
listen stats :9000 #Listen on localhost port 9000
                mode http
                stats enable #Enable statistics
                stats hide-version #Hide HAPRoxy version, a necessity for any public-facing site
                stats realm Haproxy\ Statistics #Show this text in authentication popup (escape space characters with backslash)
                stats uri /haproxy_stats #The URI of the stats page, in this case localhost:9000/haproxy_stats
                stats auth yourUsername:yourPassword #Set a username and password
```


***on ha2:*** 
>[root@ha2 ~] nano /etc/haproxy/haproxy.cfg

```
global
        log 127.0.0.1   local0
        log 127.0.0.1   local1 debug
        maxconn   45000 # Total Max Connections.
        daemon
        nbproc      1 # Number of processing cores.
defaults
        timeout server 86400000
        timeout connect 86400000
        timeout client 86400000
        timeout queue   1000s
# [HTTP Site Configuration]
listen  http_web 192.168.1.10:80
                bind *:80
                bind *:443 ssl crt /etc/ssl/iredmail.org/iredmail.org.pem
            redirect scheme https if !{ ssl_fc }
        mode http
        balance roundrobin  # Load Balancing algorithm
        option httpchk
        option forwardfor
        server mail1 192.168.1.3:80 weight 1 maxconn 512 check
        server mail2 192.168.1.4:80 weight 1 maxconn 512 check
# [HTTPS Site Configuration]
listen  https_web 192.168.1.10:443
        mode tcp
        balance source# Load Balancing algorithm
        reqadd X-Forwarded-Proto:\ http
        server mail1 192.168.1.3:443 weight 1 maxconn 512 check
        server mail2 192.168.1.4:443 weight 1 maxconn 512 check
# Reporting
listen stats :9000 #Listen on localhost port 9000
                mode http
                stats enable #Enable statistics
                stats hide-version #Hide HAPRoxy version, a necessity for any public-facing site
                stats realm Haproxy\ Statistics #Show this text in authentication popup (escape space characters with backslash)
                stats uri /haproxy_stats #The URI of the stats page, in this case localhost:9000/haproxy_stats
                stats auth yourUsername:yourPassword #Set a username and password
```

***on both servers:***  

create cert for ssl redirect (to iRedMail Servers) 
>[root@ ~] mkdir /etc/ssl/iredmail.org/  
>
>[root@ ~] openssl genrsa -out /etc/ssl/iredmail.org/iredmail.org.key 2048 
> 
>[root@ ~] openssl req -new -key /etc/ssl/iredmail.org/iredmail.org.key \
                   -out /etc/ssl/iredmail.org/iredmail.org.csr  

>[root@ ~] openssl x509 -req -days 365 -in /etc/ssl/iredmail.org/iredmail.org.csr \  
                    -signkey /etc/ssl/iredmail.org/iredmail.org.key \
                    -out /etc/ssl/iredmail.org/iredmail.org.crt  
                  
>[root@ ~] cat /etc/ssl/iredmail.org/iredmail.org.crt /etc/ssl/iredmail.org/iredmail.org.key \
           | sudo tee /etc/ssl/iredmail.org/iredmail.org.pem   


activate HAProxy service 
>[root@ ~] systemctl enable haproxy  
>[root@ ~] systemctl start haproxy

check log if any errors 
>[root@ ~] tail -f /var/log/messages

allow http/s,haproxystat  ports
>[root@ ~] firewall-cmd --zone=public --permanent --add-port=80/tcp  
>[root@ ~] firewall-cmd --zone=public --permanent --add-port=443/tcp  
>[root@ ~] firewall-cmd --zone=public --permanent --add-port=9000/tcp  
>[root@ ~] firewall-cmd --reload


# 3. Install+Configure GlusterFS #
**Install on 2 servers (mail1 & mail2)*  

***first, add new hard drive with the same capacity***  
***on both servers:***
>[root@ ~] nano /etc/hosts   
```
192.168.1.3 mail1
192.168.1.4 mail2
```

***on mail1:***   

add new disk   
type 'n', and hit enter for next question, (dont forget to write) hit 'w'
>[root@mail1 ~] fdisk /dev/sdb     
>[root@mail1 ~] /sbin/mkfs.ext4 /dev/sdb1   
>[root@mail1 ~] mkdir /glusterfs1  
>[root@mail1 ~] nano /etc/fstab   
```
/dev/sdb1 /glusterfs1      ext4    defaults        1 2
```
remount all
>[root@mail1 ~] mount -a


***on mail2:***   

add new disk   
type 'n', and hit enter for next question, (dont forget to write) hit 'w'
>[root@mail2 ~] fdisk /dev/sdb 
>[root@mail2 ~] /sbin/mkfs.ext4 /dev/sdb1   
>[root@mail2 ~] mkdir /glusterfs2   
>[root@mail2 ~] nano /etc/fstab   
```
/dev/sdb1 /glusterfs1      ext4    defaults        1 2
```
remount all
>[root@mail2 ~] mount -a



***second, install GulsterFS***

***on both servers:***
>[root@ ~] rpm  -ivh  http://dl.fedoraproject.org/pub/epel/7/x86_64/e/epel-release-7-5.noarch.rpm   
>[root@ ~] wget -P /etc/yum.repos.d http://download.gluster.org/pub/gluster/glusterfs/3.7/3.7.5/CentOS/glusterfs-epel.repo  
>[root@ ~] yum -y install glusterfs glusterfs-fuse glusterfs-server

activate the service 
>[root@ ~] systemctl enable glusterd.service  
>[root@ ~] systemctl start glusterd.service 

disabling firewall  
>[root@ ~] systemctl stop firewalld.service  
>[root@ ~] systemctl disable firewalld.service     

***on mail1:***  
>[root@mail1 ~]# gluster peer probe mail2  

***on mail2:***  
>[root@mail2 ~]# gluster peer probe mail1  

*u can check status using this command : 'gluster peer status'*


***ONLY on mail1:***
>[root@mail1 ~]# gluster volume create mailrep-volume replica 2  mail1:/glusterfs1/vmail  mail2:/glusterfs2/vmail force  
>[root@mail1 ~]# gluster volume start mailrep-volume  

check it 
>[root@mail1 ~]# gluster volume info mailrep-volume  



***create folder for vmail n mount glusterfs to vmail folder***
  
***on mail1:*** 
>[root@mail1 ~]# mkdir  /var/vmail  
>[root@mail1 ~]# mount.glusterfs mail1:/mailrep-volume /var/vmail/    
>[root@mail1 ~]# nano /etc/fstab  
```
mail1:/mailrep-volume /var/vmail glusterfs defaults,_netdev 0 0
```

remount
>[root@mail1 ~] mount -a

check it
>[root@mail1 ~] df -h  


***on mail2:*** 
>[root@mail2 ~]# mkdir  /var/vmail  
>[root@mail2 ~]# mount.glusterfs mail2:/mailrep-volume /var/vmail/  
>[root@mail2 ~]# nano /etc/fstab  
```
mail2:/mailrep-volume /var/vmail glusterfs defaults,_netdev 0 0
```

remount
>[root@mail2 ~] mount -a

check it
>[root@mail2 ~] df -h  

u can test it by creating any files on one of your mail servers
>[root@ ~] cd  /var/vmail; touch R1 R2 R3 R4 R5 R6

make sure it, by checking files on both servers
> [root@ ~] ls -la /var/vmail



# 4. Install+Configure iRedmail  #
**Install on 2 servers (mail1 & mail2)*  

For installing iredmail on centos,check this link : http://www.iredmail.org/docs/install.iredmail.on.rhel.html  

**ATTENTION**: 
* install iRedMail to mail1 first, after mail1 finish you can install it to mail2 (better,do not reboot after installing iRedMail, wait untill finish install/configure)  
* **Dont forget to choose LDAP** and using default mail folder : '/var/vmail' ---  (use nginx)



**after installing iRedMail u can go to the next step :*   

## ***Configure LDAP sync (Master-Slave)***  ##

***on mail1 (MASTER):***
>[root@mail1 ~] nano /etc/openldap/slapd.conf 

```
moduleload syncprov
index entryCSN,entryUUID eq
overlay syncprov
syncprov-checkpoint 100 10
syncprov-sessionlog 200
```


***on mail2 (SLAVE):***
>[root@mail2 ~] nano /etc/openldap/slapd.conf 
```
syncrepl   rid=001
           provider=ldap://mail1:389
           searchbase="dc=iredmail,dc=kom"
           bindmethod=simple
           binddn="cn=vmail,dc=iredmail,dc=kom"
           credentials=erec3xiThBUW9QnnU9Bnifp3434
           schemachecking=on
           type=refreshOnly
           retry="60 +"
           scope=sub
           interval=00:00:01:00
           attrs="*,+"
```


***on both servers :***  
*on both servers set firewalld to accept gluster port, ldap port, and database to each servers*   
*or u can set by your own rules*

>[root@ ~] firewall-cmd --permanent --zone=iredmail --add-rich-rule="rule family="ipv4" \
            source address="192.168.1.3/24" \
            port protocol="tcp" port="389" accept"  

>[root@ ~] firewall-cmd --permanent --zone=iredmail --add-rich-rule="rule family="ipv4" \
            source address="192.168.1.4/24" \
            port protocol="tcp" port="3306" accept"  

>[root@ ~] firewall-cmd --zone=iredmail --permanent --add-port=111/udp  
>[root@ ~] firewall-cmd --zone=iredmail --permanent --add-port=24007/tcp  
>[root@ ~] firewall-cmd --zone=iredmail --permanent --add-port=24008/tcp  
>[root@ ~] firewall-cmd --zone=iredmail --permanent --add-port=24009/tcp  
>[root@ ~] firewall-cmd --zone=iredmail --permanent --add-port=139/tcp  
>[root@ ~] firewall-cmd --zone=iredmail --permanent --add-port=445/tcp  
>[root@ ~] firewall-cmd --zone=iredmail --permanent --add-port=965/tcp  
>[root@ ~] firewall-cmd --zone=iredmail --permanent --add-port=2049/tcp  
>[root@ ~] firewall-cmd --zone=iredmail --permanent --add-port=38465-38469/tcp  
>[root@ ~] firewall-cmd --zone=iredmail --permanent --add-port=631/tcp  
>[root@ ~] firewall-cmd --zone=iredmail --permanent --add-port=963/tcp  
>[root@ ~] firewall-cmd --zone=iredmail --permanent --add-port=49152-49251/tcp  

reload it
>[root@ ~] firewall-cmd --reload  

reload ldap   
>[root@ ~] systemctl restart slapd.service


## Configure MariaDB replica (Master-Master) : ##
***on mail1 :***  
>[root@mail1 ~]# nano /etc/my.cnf

```
server-id                   = 1
    log_bin                 = /var/log/mariadb/mariadb-bin.log
    log-slave-updates
    log-bin-index           = /var/log/mariadb/log-bin.index
    log-error               = /var/log/mariadb/error.log
    relay-log               = /var/log/mariadb/relay.log
    relay-log-info-file     = /var/log/mariadb/relay-log.info
    relay-log-index         = /var/log/mariadb/relay-log.index
    auto_increment_increment = 10
    auto_increment_offset   = 1
    binlog_do_db            = amavisd
    binlog_do_db            = iredadmin
    binlog_do_db            = roundcubemail
    binlog_do_db            = sogo
    binlog-ignore-db=test
    binlog-ignore-db=information_schema
    binlog-ignore-db=mysql
    binlog-ignore-db=iredapd
    log-slave-updates
    replicate-ignore-db=test
    replicate-ignore-db=information_schema
    replicate-ignore-db=mysql
    replicate-ignore-db=iredapd
```

reload database    
>[root@mail1 ~]# systemctl restart mariadb

***on mail2 :***  
>[root@mail2 ~]# nano /etc/my.cnf
```
server-id                   = 2
    log_bin                 = /var/log/mariadb/mariadb-bin.log
    log-slave-updates
    log-bin-index           = /var/log/mariadb/log-bin.index
    log-error               = /var/log/mariadb/error.log
    relay-log               = /var/log/mariadb/relay.log
    relay-log-info-file     = /var/log/mariadb/relay-log.info
    relay-log-index         = /var/log/mariadb/relay-log.index
    auto_increment_increment = 10
    auto_increment_offset = 1
    binlog_do_db            = amavisd
    binlog_do_db            = iredadmin
    binlog_do_db            = roundcubemail
    binlog_do_db            = sogo
    binlog-ignore-db=test
    binlog-ignore-db=information_schema
    binlog-ignore-db=mysql
    binlog-ignore-db=iredapd
    log-slave-updates
    replicate-ignore-db=test
    replicate-ignore-db=information_schema
    replicate-ignore-db=mysql
    replicate-ignore-db=iredapd
```
reload database  
>[root@mail2 ~]# systemctl restart mariadb


## create replicator dbuser on both servers ##

***on mail1 :***   
```
[root@mail1 ~]# mysql -u root -pYourDBRootPassword
                create user 'replicator'@'%' identified by '12345678';
                grant replication slave on *.* to 'replicator'@'%';                                 
                SHOW MASTER STATUS; 
                +--------------------+----------+----------------------------------------------+-------------------------------+
                | File               | Position | Binlog_Do_DB                                 | Binlog_Ignore_DB              |
                +--------------------+----------+----------------------------------------------+-------------------------------+
                | mariadb-bin.000001 |      245 | amavisd,iredadmin,iredapd,roundcubemail,sogo | test,information_schema,mysql |
                +--------------------+----------+----------------------------------------------+-------------------------------+
                1 row in set (0.00 sec)                
                exit;
```
**check master status for 'File' and 'Position'*


***on mail2 :*** 
```
[root@mail2 ~]# mysql -u root -pYourDBRootPassword
                create user 'replicator'@'%' identified by '12345678';
                grant replication slave on *.* to 'replicator'@'%'; 
                slave stop;         
                CHANGE MASTER TO MASTER_HOST = '192.168.1.3', MASTER_USER = 'replicator', MASTER_PASSWORD = '12345678', MASTER_LOG_FILE = 'mariadb-bin.000001', MASTER_LOG_POS = 245; 
                slave start;                     
                SHOW MASTER STATUS;     
                +--------------------+----------+----------------------------------------------+-------------------------------+
                | File               | Position | Binlog_Do_DB                                 | Binlog_Ignore_DB              |
                +--------------------+----------+----------------------------------------------+-------------------------------+
                | mariadb-bin.000001 |     289 | amavisd,iredadmin,iredapd,roundcubemail,sogo | test,information_schema,mysql |
                +--------------------+----------+----------------------------------------------+-------------------------------+
                1 row in set (0.00 sec)
                show slave status\G;    
                exit;
```
**change to your own master status MASTER_LOG_FILE is from 'File', MASTER_LOG_POS is from 'Position' of master mail1*  
**check master status for 'File' and 'Position'*


reload database  
>[root@mail2 ~]# systemctl restart mariadb


go back to mail1   

***on mail1 :*** 
```
[root@mail1 ~]# 
                mysql -u root -pYourDBRootPassword
                slave stop;
                CHANGE MASTER TO MASTER_HOST = '192.168.1.4', MASTER_USER = 'replicator', MASTER_PASSWORD = '12345678', MASTER_LOG_FILE = 'mariadb-bin.000001', MASTER_LOG_POS = 289; 
                slave start;             
                show slave status\G;    
                exit;   
```

**change to your own master status MASTER_LOG_FILE is from 'File', MASTER_LOG_POS is from 'Position' of master mail2*  

reload database  
>[root@mail1 ~]# systemctl restart mariadb


***reboot one of mailserver n wait till up, then reboot the other mailserver (*)***  

**....FINISH....**          


**Note for testing:**

* For HA Testing, u can try to shutdown one of your server to testing it (ha1 or ha2 --/OR-- mail1 or mail2)
* u can create users using iredadmin on mail1, then check users from mail2 and you can see its already sync
* try to login using roundcubemail from any mailserver then u can check users on database 'roundcubemail->users', and its already sync
* only mail1 'can add n modify' users
* this mailservers act as Glusterserver & Glusterclient, if u want to reboot the servers, please reboot first server untill this up then reboot the second server.
* if all servers are reboot for the same time it will not mounting '/var/vmail' folder. u must force mount manually using this command 'gluster volume start mailrep-volume force'



to view the DB easily, u can install phpmyadmin (*nginx):  
> yum install phpmyadmin -y  
> ln -s /usr/share/phpMyAdmin /var/www/html/  
> cd /var/lib/php/  
> mkdir session  
> chown nginx:nginx session/  